@echo off
cls

call b.bat
if %errorlevel% NEQ 0 goto builderror

call f.bat
if %errorlevel% NEQ 0 goto flasherror

call m.bat
goto final_exit

:builderror
echo !!! build error !!!
goto final_exit

:flasherror
echo !!! flash error !!!
got final_exit

:final_exit
echo done
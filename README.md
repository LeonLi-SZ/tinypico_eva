# TinyPICO evaluate Hello World Example

Starts a FreeRTOS task to print "Hello world of TinyPICO"

See the README.md file in the upper level 'examples' directory for more information about examples.

mscTest.cpp cmscTest
mscGPIO.cpp cmscGPIO
mscRGBled.cpp cmscRGBled
mscLiteFIFO.h


project path
C:\Users\lihuamin\TinyPICO_Eva\
(below has:   folder build, folder main, file a.bat, b.bat ... etc)

ESP-IDF path
C:\TinyPICO_ESP_IDF
(below has: folder docs, folder examples, folder make, folder tools ... etc)


UART para:
Usually it's COM11 (but need to check Windows Device Manager to confirm)
115200
NONE
8
1

then you can see the TinyPICO board printf message from USB->Serial port



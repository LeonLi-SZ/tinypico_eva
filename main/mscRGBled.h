// file：mscRGBled.h

#pragma once


/*
   leon.li@mattel.com

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "allSystem.h"



/**
* @brief MSC class for control RGB LED on TinyPICO board with mutex guard
*/
class cmscRGBled {
  public:
    /**
    * @brief get a singleton instance. use    cmscRGBled &rgbled = cmscRGBled::getInstance();
    * @return reference of object itself
    */
    static cmscRGBled& getInstance(void) { // singleton
      static cmscRGBled instance; // will call constructor once only !!!
      return instance;
    }

    /**
    * @brief turn on RGB LED with previous brightness
    * @return none
    */
    inline void on(void) {
      ctrl(0, 0, 0, 0, 0);
    }

    /**
    * @brief turn on RGB LED with brightness [0, 100]
    * @param brightness [0, 100]
    * @return none
    */
    inline void on(uint8_t brightness) {
      ctrl(3, brightness, 0, 0, 0);
    }

    /**
    * @brief turn off RGB LED by setting brightness to 0
    * @return none
    */
    inline void off(void) {
      ctrl(3, 0, 0, 0, 0);
    }

    /**
    * @brief apply power to RGB LED. see TinyPICO sch
    * @return reference of object itself
    */
    inline cmscRGBled& powerUp(void) {
      ctrl(1, 0, 0, 0, 0);
      return *this;
    }

    /**
    * @brief cut off power to RGB LED. all configure inside LED lost
    * @return reference of object itself
    */
    inline cmscRGBled& powerDown(void) {
      ctrl(2, 0, 0, 0, 0);
      return *this;
    }

    /**
    * @brief set color using r, g, b respectively (!!! must call 'on' to refresh !!!)
    * @param r red
    * @param g green
    * @param b blue
    * @return none
    */
    inline void color(uint8_t r, uint8_t g, uint8_t b) {
      ctrl(4, 0, r, g, b);
    }

    inline void red(void) {color(0xFF, 0, 0), on();}
    inline void green(void) {color(0, 0xFF, 0); on();}
    inline void blue(void) {color(0, 0, 0xFF); on();}

  private:
    uint8_t bri;  // Global brightness setting [B00000, B11111] [0, 31] (mapped from [0, 100])
    uint8_t r;
    uint8_t g;
    uint8_t b;

    cmscGPIO pwr;
    cmscGPIO dat;
    cmscGPIO clk;

    void updateLED(void);  // Issue bright&color data to strip

    void swspi_init(void);      // Start bitbang SPI
    void swspi_out(uint8_t n);  // Bitbang SPI write

    /**
    * @brief control led all in one
    * @param mode 0 = refresh only; 1 = power up; 2 = power down; 3 = set brightness, 4 = set r/g/b color
    * @param brightness [0, 100]
    * @param r red [0, 0xFF]
    * @param g green [0, 0xFF]
    * @param b blue [0, 0xFF]
    * @return none
    */
    void ctrl(int mode, uint8_t brightness, uint8_t r, uint8_t g, uint8_t b);

    /**
    * @brief mutex gard
    */
    xSemaphoreHandle mutex;

  private: // singleton
    cmscRGBled(void);   // private default constructor
    cmscRGBled(const cmscRGBled&); // private copy constructor
    const cmscRGBled operator = (const cmscRGBled&); // private assignment operator
};

/* TinyPICO evaluate Example

   leon.li@mattel.com

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "allSystem.h"

/* ==============================================
!!! this file is in application layer
Not any ESP-IDF API is allowed to be used in this layer !!!

Eg:
io_conf.mode = GPIO_MODE_OUTPUT;
gpio_config(&io_conf);
gpio_get_level((gpio_num_t)gpio_no);

all those ESP-IDF API must be encapsulated in mscxxxx class, then Eg:
cmscGPIO mscGPIO(mscGPIO_pin_no::adc2ch8_dac1_g25_p14);
myTestPin.setOutput().high();

cmscGPIO yourTestpin();
yourTestpin.pinNo(mscGPIO_pin_no::adc2ch8_dac1_g25_p14).setInput().pullUp(true).pullDown(false).installIrq(....);


cmscRGBled &rgbLED = cmscRGBled::getInstance();	// singleton
rgbLED.powerUp();
regbLED.red();
...

All code here are LOGICAL, not PHYSICAL

Visual Source Code IntelliSense will prompt you class candidates in real-time
during your typing
Eg:
yourTestpin.
you will get prompt

Visual Source Code IntelliSense will prompt you enum class candidates in real-time
during your typing also
Eg:
mscGPIO_pin_no::
you will get prompt, you can type more Eg: g25, VS Code may give you candidates meet g25
Eg: adc2ch8_dac1_g25_p14

P.S. FreeRTOS API can be used in this layer,
but I still integrated some FreeRTOS elements (Eg: mutex guard) in cmscxxx class

============================================== */

#define STACK_DEPTH 2048

cmscEEPROM<std::string>username("username_ee");
cmscEEPROM<float>userscore("userscore_ee");
cmscEEPROM<int>gpio25flip("gpio25flip_ee");

// ============ demo task ============ //

#if 0 // 2 styles
cmscGPIO outpin(mscGPIO_pin_no::adc2ch8_dac1_g25_p14);
#else
cmscGPIO outpin = cmscGPIO(mscGPIO_pin_no::adc2ch8_dac1_g25_p14);
#endif

static void task_gpio25_out(void *arg) {
    std::cout<<"task_gpio25_out 1st run. output pin demo\n"; vTaskDelay(1);
    outpin.setOutput();

    printf("task_gpio25_out priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);

    while(1) {
        std::cout<<"GPIO 25 output high\n";
        outpin.high();
        vTaskDelay(3000 / portTICK_PERIOD_MS);

        std::cout<<"GPIO 25 output low\n";
        outpin.low();
        vTaskDelay(3000 / portTICK_PERIOD_MS);
    }
}

// ============ demo task ============ //

#if 0 // 2 styles
static cmscGPIO inpin(mscGPIO_pin_no::adc2ch9_dac2_g26_p15);
#else
static cmscGPIO inpin = cmscGPIO(mscGPIO_pin_no::adc2ch9_dac2_g26_p15);
#endif

static void task_gpio26_in(void *arg) {
    std::cout<<"task_gpio26_in 1st run. input pin demo\n"; vTaskDelay(1);
    inpin.setInput().pullUp(true);

    printf("task_gpio26_in priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);

    int tmp;
    volatile int previous = -1;

    while(1) {
        vTaskDelay(300 / portTICK_PERIOD_MS);

        tmp = inpin.readPin();
        if(previous != tmp) {
            previous = tmp;
            printf("--- gpio 26 input status changed to = %d\n", tmp);
        }
    }
}

// ============ demo task ============ //

static void task_rgbled(void *arg) {
    std::cout<<"task_rgbled 1st run\n"; vTaskDelay(1);
    printf("task_rgbled priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);

    cmscRGBled &rgbled = cmscRGBled::getInstance().powerUp();

    // a demo pattern
    const static uint8_t colortbl [][3] = {
        {0xFF, 0, 0},   // red
        {0, 0xFF, 0},   // green
        {0, 0, 0xFF},   // blue
        {0xFF, 0xFF, 0xFF}, // white
        {0, 0, 0},      // dark
    };

    int i = 0;
    rgbled.on(5); // brightness 5
    while(1) {
        rgbled.color(colortbl[i][0], colortbl[i][1], colortbl[i][2]);
        vTaskDelay(800 / portTICK_PERIOD_MS);
        if(++i > 4) i = 0;
    }
}

// ============ demo task ============ //
// Eg: used to store g sensor data... x,y,z...and more...

static bool isFaster = false; // for easy demo speed up FIFO in/out buffer

#pragma pack(1)
typedef struct {
    int id;
    float value;
}demo_data_t;
#pragma pack()

cmscLiteFIFOmutex<demo_data_t> demofifo;

static void task_fifo_in_test(void *arg) {
    printf("task_fifo_in_test(%d) 1st run\n", (int)arg) ; vTaskDelay(1);
    printf("task_fifo_in_test(%d) priority = %d\n", (int)arg, uxTaskPriorityGet(NULL)); vTaskDelay(1);

    const static demo_data_t demo_data[] = {
        {	1	,	88.5	} ,
        {	2	,	-83.1	} ,
        {	3	,	-82.3	} ,
        {	4	,	17.3	} ,
        {	5	,	16.7	} ,
        {	6	,	-84.2	} ,
        {	7	,	78.1	} ,
        {	8	,	-88.4	} ,
        {	9	,	-51.5	} ,
        {	10	,	23.8	} ,
        {-1, 0} // -1 is end flag
    };

    demofifo.in(demo_data[0]);
    demofifo.in(demo_data[1]);
    demofifo.in(demo_data[2]);
    printf("(1) size %d\n", demofifo.size()); // should be 3
    std::cout<<"\n\nabout to clear FIFO\n\n"; vTaskDelay(1);
    demofifo.clear();   // test clear() here
    printf("(2) size %d\n", demofifo.size()); // should be 0

    vTaskDelay(5000 / portTICK_PERIOD_MS);

    const demo_data_t *p = demo_data;
    while(1) {
        demo_data_t tmp = *p;
        if(tmp.id == -1) {
            p = demo_data;
            continue;
        }
        demofifo.in(tmp);
        printf("fifo_in (%d) %d | %.2f\n", demofifo.size(), tmp.id, tmp.value);
        p++;

        // in is a little bit faster than out. then system will be dead finally
		// sometimes we'd like to know when system is dead as a stress test
        if(isFaster) {
            vTaskDelay(110 / portTICK_PERIOD_MS);
        }else{
            vTaskDelay(2345 / portTICK_PERIOD_MS);
        }

        switch ((int)arg) {
            case 1 : {
                if(demofifo.size() > 1000) {
                    std::cout<<"\n\n-------- fifo in 1 deleted\n\n";
                    isFaster = false;
                    vTaskDelete(NULL);
                }
                break;
            }
            case 2 : {
                if(demofifo.size() > 1020) {
                    std::cout<<"\n\n-------- fifo in 2 deleted\n\n";
                    isFaster = true;
                    vTaskDelete(NULL);
                }
                break;
            }
        }
    }
}

static void task_fifo_out_test(void *arg) {
    std::cout<<"task_fifo_out_test 1st run\n"; vTaskDelay(1);
    printf("task_fifo_out_test priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);

    int cnt = 0;

    while(1) {
        demo_data_t tmp = demofifo.out();
        printf("fifo_out (%d) %d | %.2f\n", demofifo.size(), tmp.id, tmp.value);

        // in is a little bit faster than out. then system will be dead finally
        // we'd like to know when system is dead as a stress test
        if(isFaster) {
            vTaskDelay(215 / portTICK_PERIOD_MS);
        }else{
            vTaskDelay(4567 / portTICK_PERIOD_MS);
        }

        if(demofifo.size() == 0) {
            if(++cnt > 200) {
                std::cout<<"\n\n\n----- fifo out deleted\n\n";
                vTaskDelete(NULL);
            }
        }
    }
}

// ============ demo task ============ //

xQueueHandle xQueGpio_21_5;

static void gpio_isr_21_5(void *arg);

static void task_gpio_21_5_in(void *arg) {
    std::cout<<"task_gpio_21_5_in 1st run\n"; vTaskDelay(1);
    printf("task_gpio_21_5_in priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);
    printf("sizeof(uint32_t) = %d\n", sizeof(uint32_t));

    // max 4 message.
    xQueGpio_21_5 = xQueueCreate(4, sizeof(uint32_t));

    cmscGPIO irqPinG21(mscGPIO_pin_no::sda_g21_p42);
    irqPinG21.setInput().pullUp(true).pullDown(false);

    cmscGPIO irqPinG5(mscGPIO_pin_no::ss_g5_p34);
    irqPinG5.setInput().pullUp(true).pullDown(false);

    static int pre21 = -1;
    static int pre5 = -1;

    irqPinG21.installIrq(gpio_isr_21_5, mscGPIO_irq_type::both_edge, mscGPIO_irq_level::level3);
    irqPinG5.installIrq(gpio_isr_21_5, mscGPIO_irq_type::both_edge, mscGPIO_irq_level::level3);

    while(1) {
        uint32_t irqPinNum = -1;
        xQueueReceive(xQueGpio_21_5, &irqPinNum, (8 * 1000 / portTICK_PERIOD_MS)); // 8 seconds tmo (max blcoked 8 sec)
        // if Que received, go to here at once. If no Que received, go to here every 8 seconds
        // which means, even if somehow missed an irq, this mechanism still has chance to show correct pin level after max. 8 seconds

        switch (irqPinNum) {
            case 5: {
                irqPinG5.disablePinIrq();
                vTaskDelay(200 / portTICK_PERIOD_MS); // put your desired debounce delay here
                irqPinG5.enablePinIrq(mscGPIO_irq_type::both_edge);
                break;
            }
            case 21: {
                irqPinG21.disablePinIrq();
                vTaskDelay(200 / portTICK_PERIOD_MS); // put your desired debounce delay here
                irqPinG21.enablePinIrq(mscGPIO_irq_type::both_edge);
                break;
            }
        }

        // read pin anyway
        int cur = irqPinG5.readPin(); // only use irqPinNum for dis/ena irq as above. use readpin() to get actual pin level after debounce dealy
        if(pre5 != cur) {
            pre5 = cur;
            printf("GPIO5 (dual mode detect) input status changed to = %d\n", cur); // polling + irq
        }

        cur = irqPinG21.readPin();
        if(pre21 != cur) {
            pre21 = cur;
            printf("GPIO21 (dual mode detect) input status changed to = %d\n", cur); // pollin + irq
        }
    }
}

// ============ demo task ============ //

static void task_iic(void *arg) {
    std::cout<<"task_iic 1st run\n"; vTaskDelay(1);
    printf("task_iic priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);

    while(1) {
        // .... TODO ...

        vTaskDelay(1000 / portTICK_PERIOD_MS);
        // ================= //
        std::cout<<"\nabout to delete task of task_iic...\n\n";
        vTaskDelete(NULL);
    }
}

// ============ demo task ============ //

static void task_show_cr(void *arg) {
    _cmscCopyright_::show();
    std::cout<<_cmscCopyright_::wifissid()<<std::endl;

    for(int i = 5; i > 0; i--) {
        vTaskDelay(10 * 1000 / portTICK_PERIOD_MS);
        _cmscCopyright_::show();
    }

    vTaskDelay(10 * 1000 / portTICK_PERIOD_MS);
    std::cout<<"\a\n\n ------ one more fifo in task\n\n";
    xTaskCreate(task_fifo_in_test, "fifoin2", STACK_DEPTH, (int*)2, 8, NULL); // 2nd fifoin
    isFaster = true;

    while(1) {
        vTaskDelay(6701); // a prime for 67 seconds (67 also a prime)
        _cmscCopyright_::show();
    }
}

// ============ demo task ============ //

static void task_light_deep_sleep(void *arg) {
    std::cout<<"task_light_deep_sleep 1st run\n"; vTaskDelay(1);
    printf("task_light_deep_sleep priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);

    cmscSleep &sleep = cmscSleep::getInstance().configDwnCnt(20, 30);
    cmscRGBled &rgbled = cmscRGBled::getInstance();

    while(1) {
        int ls_cnt = 0; // counts light sleep
        while(1) { // dead loop for light sleep
            vTaskDelay(1000 / portTICK_PERIOD_MS);

            if(sleep.dec_light_cnt() == 0) { // down count to 0, then light sleep
                std::cout<<"\a\n\n\nabout to light sleep...\n\n\nwill wakeup by gpio26 low level or 10 sec tmo\n";
                sleep.select_wakeup_pin(inpin, mscGPIO_wakeup_type::low_level).set_wakeup_timer(10*1000*1000);

                rgbled.powerDown();

                while(inpin.readPin() == 0) { // wait pin goes high
                    vTaskDelay(100);
                }

                int64_t t1 = sleep.start_light_sleep();
                std::cout<<"light sleep for "<<t1<<" us\n";

                sleep.disable_all_wakeup_source().reset_light_cnt().show_wakeup_source();
                inpin.disablePinWakeup();
                rgbled.powerUp().on(5);

                printf("light sleep %d times\n", ++ls_cnt);
                if(ls_cnt >= 2) {
                    std::cout<<"next round will demo deep sleep\n";
                    break; // to deep sleep while(1)
                }
            }
        }

        while(1) { // dead loop for deep sleep
            vTaskDelay(1000 / portTICK_PERIOD_MS);

            if(sleep.dec_deep_cnt() == 0) { // down count to 0, then deep sleep
                std::cout<<"\a\n\n\nabout to deep sleep...\n\n\nwill wakeup by gpio26 low level or 30 sec tmo\n";
                
                int flip = gpio25flip;
                if((flip & 1) == 0) {
                    outpin.low();
                    std::cout<<"GPIO 25 output low before deep sleep\n";
                }else{
                    outpin.high();
                    std::cout<<"GPIO 25 output high before deep sleep\n";
                }

                sleep.enable_gpio_hold(inpin).set_wakeup_timer(30*1000*1000).enable_singlepin_deep_sleep_wakeup(inpin, mscSinglepin_wakeup_level::low);

                rgbled.powerDown();

                while(inpin.readPin() == 0) { // wait pin goes high
                    vTaskDelay(100);
                }

                sleep.start_deep_sleep();

                // ... chip will be reboot and run from begining ...
                // but _cmscPwrUp_::reset_reason(); will show
                // ESP reset reason: exiting deep sleep : 8

                // will NEVEL execute below code
                std::cout<<"!!! you will NEVER see this line !!!\n";
                break; // will NEVEL be executed
            }
        }
    }
}

// ============ demo task ============ //

static void task_gpio27_out_in(void *arg) {
    std::cout<<"task_gpio27_out_in 1st run\n"; vTaskDelay(1);
    printf("task_gpio27_out_in priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);

    cmscGPIO pinG27(mscGPIO_pin_no::adc2ch7_touch7_g27_p16);
    pinG27.setOutput();

    int cnt = 0;

    while(1) {
        std::cout<<"gpio 27 output high\n";
        pinG27.high();
        vTaskDelay(4000 / portTICK_PERIOD_MS);

        std::cout<<"gpio 27 output low\n";
        pinG27.low();
        vTaskDelay(4000 / portTICK_PERIOD_MS);

        if(++cnt == 10) {
            std::cout<<"\a\ngpio 27 changed to input mode\n";
            break;
        }
    }

    pinG27.setInput().pullUp(true).pullDown(false);
    cnt = -1; // tmp use cnt as pin level
    while(1) {
        vTaskDelay(500 / portTICK_PERIOD_MS);
        int level = pinG27.readPin();
        if(cnt != level) {
            cnt = level;
            std::cout<<"---- gpio 27 level change to "<<cnt<<std::endl;
        }
    }
}

// =========================== app_main ============================== //

#ifdef __cplusplus
extern "C" {
#endif

void app_main() {
    std::cout<<"\a\n\nTinyPICO_Eva: Hello world of TinyPICO!\n";
    std::cout<<"TinyPICO evaluate. Mattel. MSC SZIH.\n";
    printf("\nconfigMAX_PRIORITIES = %d\n", configMAX_PRIORITIES); // test result shows configMAX_PRIORITIES = 25
    printf("portTICK_PERIOD_MS = %d\n", portTICK_PERIOD_MS); // test result shows portTICK_PERIOD_MS = 10
    printf("configUSE_MUTEXES = %d\n", configUSE_MUTEXES);  // test result shows = 1
    printf("\napp_main priority = %d\n", uxTaskPriorityGet(NULL));

    extern int esp_clk_cpu_freq(void);
    extern int esp_clk_xtal_freq(void);
    printf("\nesp_clk_cpu_freq = %d MHz\n", esp_clk_cpu_freq() / 1000000); // test result shows = 160MHz
    printf("esp_clk_xtal_freq = %d MHz\n", esp_clk_xtal_freq() / 1000000); // test result shows = 40MHz

    printf("\nbuild time: %s, %s\n\n", __DATE__, __TIME__);

    // ============================== //

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
    std::cout<<"\n\n";

    // ============================== //
    _cmscPwrUp_::set();
    _cmscPwrUp_::reset_reason();

    std::cout<<"\nabout to create test/demo tasks...\n";
    vTaskDelay(3 * 1000 / portTICK_PERIOD_MS);

    // 10, 11, 2, 3, 4, 1 etc... is priority:  bigger number for higher priority
    xTaskCreate(task_gpio25_out, "gpio25out", STACK_DEPTH, NULL, 2, NULL); vTaskDelay(1);
    xTaskCreate(task_gpio26_in, "gpio26in", STACK_DEPTH, NULL, 3, NULL); vTaskDelay(1);
    xTaskCreate(task_gpio27_out_in, "gpio27outin", STACK_DEPTH, NULL, 8, NULL); vTaskDelay(1);
//    xTaskCreate(task_fifo_in_test, "fifoin1", STACK_DEPTH, (int*)1, 4, NULL); vTaskDelay(1); // will add one more fifo_in later in "task_show_cr"
 //   xTaskCreate(task_fifo_out_test, "fifoout", STACK_DEPTH, NULL, 5, NULL); vTaskDelay(1);
//    xTaskCreate(task_show_cr, "   ", STACK_DEPTH, NULL, 1, NULL); vTaskDelay(1);
    xTaskCreate(task_gpio_21_5_in, "gpio_21_5irq", STACK_DEPTH, NULL, 6, NULL); vTaskDelay(1);
 //   xTaskCreate(task_iic, "iic", STACK_DEPTH, NULL, 7, NULL); vTaskDelay(1);
    xTaskCreate(task_rgbled, "rgbled", STACK_DEPTH, NULL, 1, NULL); vTaskDelay(1);
    xTaskCreate(task_light_deep_sleep, "light_dark_sleep", STACK_DEPTH, NULL, 1, NULL); vTaskDelay(1);

    // ================= //

    cmscEEPROM<int>pwrcnt("pwrcnt_ee");
    int cnt = pwrcnt;
    pwrcnt = ++cnt;
    printf("\n\npower up %d time(s)\n\n", cnt);

    std::string name = username;
    if(name == CMSC_EEPROM_STR_NOT_FOUND) {
        std::cout<<"username not found\n";
        name = std::string("Stephen Chow");
        username = name;
    }else {
        std::cout<<"username = "<<name<<std::endl;
    }

    float score = userscore;
    if(score < 10) {
        userscore = 50.0;
    }else{
        score += 0.5;
        userscore = score;
        std::cout<<"good good study, day day up. score = "<<score<<std::endl;
    }

    cnt = gpio25flip;
    gpio25flip = ++cnt;

    // ================= //
    std::cout<<"\n\n!!!! ==== about to delete task of app_main... ==== !!!!\n\n";
    vTaskDelete(NULL);
    std::cout<<"!!! you will NEVER see this line !!!\n";
}

#ifdef __cplusplus
}
#endif

#undef STACK_DEPTH

// strange: this routine cause Visual Source Code IntelliSense NOT working for the code below it.  (IRAM_ATTR causes the problem I think)
// So  I put it at the end of the file
#if 0
static void IRAM_ATTR gpio_isr_21_5(void *arg) {
    static uint32_t gpio_num;

    gpio_num = (uint32_t)arg; // arg contains the pin # which has irq
    xQueueSendFromISR(xQueGpio_21_5, &gpio_num, NULL);
}
#else
static void gpio_isr_21_5(void *arg) { // confirm irq routine without IRAM_ATTR still works
    static uint32_t gpio_num;

    gpio_num = (uint32_t)arg; // arg contains the pin # which has irq
    xQueueSendFromISR(xQueGpio_21_5, &gpio_num, NULL);
}
#endif

// file: unused_code.cpp

/* ===============================
some code are for test purpose
in TinyPICO_Eva_main.cpp

if it's not used anymore after test.
move them to this file for future
reference

this file is not added to the project
=============================== */
#if 0
// =================================================================================

// == Note 01 ====================== //

xTaskHandle task_gpio21_in_handle;
xTaskCreate(task_gpio21_in, "gpio21irq", STACK_DEPTH, NULL, 4, &task_gpio21_in_handle);

static void task_gpio21_in(void *arg) {
    vTaskSuspend(NULL);
    ...
}

static void IRAM_ATTR gpio_isr_21(void *arg) {
    vTaskResume(task_gpio21_in_handle);
}

// == Note 02 ====================== //


// why it's
gpio_num = (uint32_t)arg;
// not
gpio_num = *((uint32_t*)arg); // convert void* arg to uint32_t*, then use * again to get the uint32_t  ?

// actually, here ESP-IDF uses a tricky, called:  "use pointer to carry value"

// formally, should be like this   (solution 1)
void receiver(void *arg) {
    uint32_t rxContent;

    rxContent = *((uint32_t*)arg);
    std::cout<<"rxContent = "<<rxContent<<std::endl;
}

void sender(void) {
    std::cout<<"formal way\n";
    static uint32_t contentToSend;
    contentToSend = 5;

    receiver((void*)&contentToSend);
}

// output is 5
// but you need define a static var inside sender
// it's a little bit wasting RAM

// trickly, can be like this   (solution 2)
void receiver(void *arg) {
    uint32_t rxContent;

    rxContent = (uint64_t)(arg); // test in PC 64 bit platform. so use uint64_t.
    // otherwise, error: cast from 'void*' to 'uint32_t {aka unsigned int}' loses precision [-fpermissive]
    // in TinyPICO, use uint32_t then
    std::cout<<"rxContent = "<<rxContent<<std::endl;
}

void sender(void) {
    std::cout<<"tricky way\n";

    receiver((void*)6); // in embedded system, actually this '6' is stored in Flash
}

// output is 6
// and you don't need to define a static var inside sender

// if you don't want to waste RAM, and still want to formal way. you can do this way (solution 3)
void receiver(void *arg) {
    uint32_t rxContent;

    rxContent = *((uint32_t*)arg);
    std::cout<<"rxContent = "<<rxContent<<std::endl;
}

void sender(void) {

    std::cout<<"formal way, but no RAM wasting\n";
    const static uint32_t contentToSend = 7;    // in embedded system, const var will be stored in Flash
    receiver((void*)&contentToSend);
}

// output is 7
// I prefer "(solution 3)"
// but ESP-IDF is using solution 1 for GPIO irq callback. you have no choice


// == Note 03 ====================== //

static void task_gpio_21_5_in(void *arg) {
    printf("task_gpio_21_5_in 1st run.\n");
    printf("task_gpio_21_5_in priority = %d\n", uxTaskPriorityGet(NULL));
    printf("sizeof(uint32_t) = %d\n", sizeof(uint32_t));

    // max 4 message.
    xQueGpio_21_5 = xQueueCreate(4, sizeof(uint32_t));
    if(xQueGpio_21_5 != 0) {
        printf("task_gpio_21_5_in xQueGpio_21_5 created\n");
    }

    cmscGPIO irqPin21(21);
    irqPin21.setInput().pullUp(true).pullDown(false);

    cmscGPIO irqPin5(5);
    irqPin5.setInput().pullUp(true).pullDown(false);


    static int pre21 = -1;
    static int pre5 = -1;
    uint32_t irqPinNum = 1;

    irqPin21.installIrq(gpio_isr_21_5, mscGPIO_irq_type::both_edge, mscGPIO_irq_level::level3);
    irqPin5.installIrq(gpio_isr_21_5, mscGPIO_irq_type::both_edge, mscGPIO_irq_level::level3);

    while(1) {
        if(xQueueReceive(xQueGpio_21_5, &irqPinNum, portMAX_DELAY)) { // Note: if no Que received, will blocked for ever (cuz you use portMAX_DELAY)
            // if received some Que will come to here
            int cur;
            switch (irqPinNum) {
                case 5: {
                    irqPin5.disablePinIrq();
                    vTaskDelay(200 / portTICK_PERIOD_MS); // put your desired debounce delay here
                    cur = irqPin5.readPin();
                    if(pre5 != cur) {
                        pre5 = cur;
                        printf("GPIO5 irq found, input status changed to = %d\n", cur);
                    }
                    irqPin5.enablePinIrq(mscGPIO_irq_type::both_edge);
                    break;
                }
                case 21: {
                    irqPin21.disablePinIrq();
                    vTaskDelay(200 / portTICK_PERIOD_MS); // put your desired debounce delay here
                    cur = irqPin21.readPin();
                    if(pre21 != cur) {
                        pre21 = cur;
                        printf("GPIO21 irq found, input status changed to = %d\n", cur);
                    }
                    irqPin21.enablePinIrq(mscGPIO_irq_type::both_edge);
                    break;
                }
            }
        }else{
            // if no Que recieved, will not go to here. cuz xQueueReceive will block for ever(cuz you use portMAX_DELAY)
            // it's just for demo
            printf("!!! (task_gpio_21_5_in) You will NEVER see this line !!!\n");
        }
    }
}

// == Note 04 ====================== //

#include <string>       // http://www.cplusplus.com/reference/string/string/

#if 1
    s1 = "--- std::string test (copy) ---\n";
    char *buf;
    buf = new char[s1.size()+1];
    s1.copy(buf, s1.size(), 0);  //size_t copy (char* s, size_t len, size_t pos = 0) const;
    buf[s1.size()] = '\0';
    printf("%s", buf);  // guarantee to be printed togeter
    delete []buf;
    buf = NULL;
#else
    s1 = "--- std::string test (for) ---\n";
    for(int i = 0; i < s1.size(); i++) {
        printf("%c", s1[i]);  // NOT guarantee to be printed togeter
    }
#endif


void printString(std::string str) {
    char *buf = NULL;
    buf = new char[str.size() + 1];
    if(buf == NULL) {
        printf("!!! printString. new buffer ERROR !!!\n");
        return;
    }

    str.copy(buf, str.size() + 1); //size_t copy (char* s, size_t len, size_t pos = 0) const;
    buf[str.size()] = '\0';
    printf("%s", buf); // guarantee to be printed togeter
    delete[] buf;
}


// == Note 05 ====================== //
 std::string name_str = "Stephen Chow";  NO working yet
printf("size of name_str = %d\n", sizeof(name_str));
name.write(std::string(name_str));

 std::string name_read;  NO working yet
printf("size of name_read = %d\n", sizeof(name_read));
name_read = name.read();

            

// == Note 06 ====================== //

xTaskCreate(task_eeprom, "eeprom", STACK_DEPTH, NULL, 7, NULL);

#pragma pack(1)
typedef struct {
    char demostr[50];
    int ix;
    float fy;
}demo_eeprom_data_t;
#pragma pack()

static void task_cnt1min(void *arg);

// EEPROM var define in global area
// make sure you must define at least
// 1 more in the local area
// before write/read them
cmscEEPROM<int>cnt1min("cnt1min_ee");

static void task_eeprom(void *arg) {
    std::cout<<"task_eeprom 1st run\n"; vTaskDelay(1);
    printf("task_eeprom priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);

    while (1) {
        cmscEEPROM<std::string>name("name_ee"); vTaskDelay(1);
        cmscEEPROM<int>id("id_ee"); vTaskDelay(1);
        cmscEEPROM<int>cnt("cnt_ee"); vTaskDelay(1);
        cmscEEPROM<float>score("score_ee"); vTaskDelay(1);

        printf("about to read EEPROM ...\n");
        std::string name_rd = name; vTaskDelay(1);
        int id_rd = id; vTaskDelay(1);
        int cnt_rd = cnt; vTaskDelay(1);
        float score_rd = score; vTaskDelay(1);
        printf("EEPROM read finished\n"); vTaskDelay(1);

        if(name_rd == CMSC_EEPROM_STR_NOT_FOUND) {
            vTaskDelay(1);
            printf("about to init. write EEPROM ...\n");
            name = std::string("Stephen Chow"); vTaskDelay(1);
            id = 9527; vTaskDelay(1);
            cnt = 0; vTaskDelay(1);
            score = 59.5; vTaskDelay(1);
            cnt1min = 0; vTaskDelay(1);
            printf("EEPROM init. write finished\n"); vTaskDelay(1);
        }else{
            std::cout<<name_rd<<std::endl; vTaskDelay(1);
            printf("%d: good good study, day day up %d time(s), score = %.2f\n",\
                    id_rd, cnt_rd, score_rd); vTaskDelay(1);
            printf("\n\ntotal running %d minute(s)\n\n", (int)cnt1min); vTaskDelay(1);

            printf("about to re-write EEPROM ...\n"); vTaskDelay(1);
            cnt = ++cnt_rd; vTaskDelay(1);
            score = score_rd + 0.5; vTaskDelay(1);
            printf("EEPROM re-write finished\n");
        }

        vTaskDelay(100);
        std::cout<<"about to create task_cnt1min\n";
        xTaskCreate(task_cnt1min, "cnt1min", STACK_DEPTH, NULL, 1, NULL);
        // ================= //
        std::cout<<"\nabout to delete task of task_eeprom_demo...\n\n";
        vTaskDelete(NULL);
    }
}

static void task_cnt1min(void *arg) {
    std::cout<<"task_cnt1min 1st run\n"; vTaskDelay(1);
    printf("task_cnt1min priority = %d\n", uxTaskPriorityGet(NULL)); vTaskDelay(1);

    int cnt = 0;

    while(1) {
        vTaskDelay(60 * 1000 / portTICK_PERIOD_MS);
        cnt++;
        cnt1min = (cnt1min + 1);
        printf("\n\n\nrunning %d minute(s), total %d minute(s)\n\n\n", cnt, (int)cnt1min);

        if(cnt == 3) {
            isFaster = true; // speed up FIFO in/out test
            xTaskCreate(task_fifo_in_test, "fifoin2", STACK_DEPTH, (int*)2, 12, NULL);  // one more 'in'
            std::cout<<"\n\n\none more 'in' for FIFO stress test. and run much faster...\n\n\n";
            std::cout<<"(c)..."<<"task_fifo_in_test\n";
        }
    }
}


// == Note 07 ====================== //


// =================================================================================

#endif


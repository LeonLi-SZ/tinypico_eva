// file：mscSleep.h

#pragma once


/*
   leon.li@mattel.com

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*
use below contents from ESP-IDF API code for mscWakeup_source definition
typedef enum {
    ESP_SLEEP_WAKEUP_UNDEFINED,    //!< In case of deep sleep, reset was not caused by exit from deep sleep
    ESP_SLEEP_WAKEUP_ALL,          //!< Not a wakeup cause, used to disable all wakeup sources with esp_sleep_disable_wakeup_source
    ESP_SLEEP_WAKEUP_EXT0,         //!< Wakeup caused by external signal using RTC_IO
    ESP_SLEEP_WAKEUP_EXT1,         //!< Wakeup caused by external signal using RTC_CNTL
    ESP_SLEEP_WAKEUP_TIMER,        //!< Wakeup caused by timer
    ESP_SLEEP_WAKEUP_TOUCHPAD,     //!< Wakeup caused by touchpad
    ESP_SLEEP_WAKEUP_ULP,          //!< Wakeup caused by ULP program
    ESP_SLEEP_WAKEUP_GPIO,         //!< Wakeup caused by GPIO (light sleep only)
    ESP_SLEEP_WAKEUP_UART,         //!< Wakeup caused by UART (light sleep only)
} esp_sleep_source_t;
*/

/**
* @brief sleep wakeup source
*/
enum class mscWakeup_source {
   UNDEFINED = ESP_SLEEP_WAKEUP_UNDEFINED,
   EXT0 = ESP_SLEEP_WAKEUP_EXT0,
   EXT1 = ESP_SLEEP_WAKEUP_EXT1,
   TIMER = ESP_SLEEP_WAKEUP_TIMER,
   TOUCHPAD = ESP_SLEEP_WAKEUP_TOUCHPAD,
   ULP = ESP_SLEEP_WAKEUP_ULP,
   GPIO = ESP_SLEEP_WAKEUP_GPIO,
   UART = ESP_SLEEP_WAKEUP_UART,
};

/*
use below contents from ESP-IDF API code for mscWakeup_source definition
typedef enum {
    ESP_EXT1_WAKEUP_ALL_LOW = 0,    //!< Wake the chip when all selected GPIOs go low
    ESP_EXT1_WAKEUP_ANY_HIGH = 1    //!< Wake the chip when any of the selected GPIOs go high
} esp_sleep_ext1_wakeup_mode_t;
*/

/**
* @brief multiple pins wake up mode from deep sleep.
* I don't know why there is no any_low option for ESP-IDF API
*/
enum class mscMultipin_deepsleep_wakeup_mode {
   all_low = ESP_EXT1_WAKEUP_ALL_LOW,
   any_high = ESP_EXT1_WAKEUP_ANY_HIGH,
};


/**
* @brief MSC class for both light and deep sleep control
*/
class cmscSleep {
   public:
      /**
       * @brief get a singleton instance. use    cmscSleep &sleep = cmscSleep::getInstance();
       * @return reference of object itself
       */
      static cmscSleep& getInstance(void) { // singleton
         static cmscSleep instance; // will call constructor once only !!!
         return instance;
      }

      /**
      * @brief provide an alternative way to select a wakeup pin. It just calls gpio's enablePinWakeup().
      * both for all IOs: RTC IOs and digital IOs. but only for light sleep wake up.
      * more detail refer ESP-IDF esp_sleep.h.
      * call: disable_all_wakeup_source to disable all wakeup source.
      * we didn't provide such as deselect_wakeup_pin
      * @param pin specify pin
      * @param type low or high
      * @return reference of object itself
      */
      static inline cmscSleep& select_wakeup_pin(cmscGPIO pin, mscGPIO_wakeup_type type) {
         pin.enablePinWakeup(type);
         return getInstance();
      }

      /**
      * @brief disable all wake up source. Eg: ext0, ext1, timer, gpio, uart...
      * @return reference of object itself
      */
      static inline cmscSleep& disable_all_wakeup_source(void) {
         esp_sleep_disable_wakeup_source(ESP_SLEEP_WAKEUP_ALL);
         return getInstance();
      }

      /**
      * @brief Enable wakeup by timer,
      * @param us time before wakeup, in micro seconds (2^64 us = 584 k years)
      * @return reference of object itself
      */
      static inline cmscSleep& set_wakeup_timer(uint64_t us) {
         esp_sleep_enable_timer_wakeup(us);
         return getInstance();
      }

      /**
      * @brief provide an alternative way to enable gpio hold. It just calls gpio's enablePinHold().
      * in output mode: the output level of the pad will be force locked and can not be changed.
      * in input mode: the input value read will not change, regardless the changes of input signal.
      * @param pin specify pin
      * @return reference of object itself
      */
      static inline cmscSleep& enable_gpio_hold(cmscGPIO pin) {
         pin.enablePinHold();
         return getInstance();
      }

      /**
      * @brief provide an alternative way to disable gpio hold. It just calls gpio's disablePinHold().
      * @param pin specify pin
      * @return reference of object itself
      */
      static inline cmscSleep& disable_gpio_hold(cmscGPIO pin) {
         pin.disablePinHold();
         return getInstance();
      }

      /**
      * @brief Enable all digital gpio pad hold function during Deep-sleep.
      * @return reference of object itself
      */
      static inline cmscSleep& enable_all_gpio_hold(void) {
         gpio_deep_sleep_hold_en();
         return getInstance();
      }

      /**
      * @brief Disable all digital gpio pad hold function during Deep-sleep.
      * @return reference of object itself
      */
      static inline cmscSleep& disable_all_gpio_hold(void) {
         gpio_deep_sleep_hold_dis();
         return getInstance();
      }

      /**
      * @brief provide an alternative way to enable a single pin wake up during Deep-sleep. It just call gpio's enablePinWakeupDeepSleep().
      * @param pin specify pin
      * @param level low or high
      * @return reference of object itself
      */
      static inline cmscSleep& enable_singlepin_deep_sleep_wakeup(cmscGPIO pin, mscSinglepin_wakeup_level level) {
         pin.enablePinWakeupDeepSleep(level);
         return getInstance();
      }

      /**
      * @brief enable a multiple pins wake up during Deep-sleep.
      * only for RTC IOs (0,2,4,12-15,25-27,32-39).
      * @param pinmask multiple pins mask. Eg: cmscSleep::enable_multipins_deep_sleep_wakeup( bitmask(25) | bitmask(26), mscMultipin_deepsleep_wakeup_mode::all_low );
      * @param mode all low or any high
      * @return reference of object itself
      */
      static inline cmscSleep& enable_multipins_deep_sleep_wakeup(uint64_t pinmask, mscMultipin_deepsleep_wakeup_mode mode) {
         esp_sleep_enable_ext1_wakeup(pinmask, (esp_sleep_ext1_wakeup_mode_t)mode);
         return getInstance();
      }

      /**
      * @brief show the wake up source (who wake up uC)
      * @return code of wakeup source
      */
      static mscWakeup_source show_wakeup_source(void);

      /**
      * @brief max down count to sleep
      * @param light_max max down count for light sleep
      * @param deep_max max down count for deep sleep
      * @return reference of object itself
      */
      static cmscSleep& configDwnCnt(unsigned int light_max, unsigned int deep_max) {
         take();
         cmscSleep::light_max = light_max;
         cmscSleep::deep_max = deep_max;
         cmscSleep::light_cnt = light_max;
         cmscSleep::deep_cnt = deep_max;
         give();
         printf("cmscSleep::configDwnCnt, light_max = %d, deep_max = %d\n", light_max, deep_max);
         return getInstance();
      }

      /**
      * @brief reset light sleep down counter to preset max value
      * @return reference of object itself
      */
      static cmscSleep& reset_light_cnt(void) {
         take();
         light_cnt = light_max;
         give();
         return getInstance();
      }

      /**
      * @brief reset deep sleep down counter to preset max value
      * @return reference of object itself
      */
      static cmscSleep& reset_deep_cnt(void) {
         take();
         deep_cnt = deep_max;
         give();
         return getInstance();
      }

      /**
      * @brief if light sleep down conter > 0, then dec it by 1
      * @return counter val after dec
      */
      static int dec_light_cnt(void)   {
         take();
         if(light_cnt > 0) {
            light_cnt--;
         }
         int rc = light_cnt;
         give();
         return rc;
      }

      /**
      * @brief if deep sleep down conter > 0, then dec it by 1
      * @return counter val after dec
      */
      static int dec_deep_cnt(void)   {
         take();
         if(deep_cnt > 0) {
            deep_cnt--;
         }
         int rc = deep_cnt;
         give();
         return rc;
      }

      /**
      * @brief start light sleep
      * @return how long stays in light sleep (micro seconds)
      */
      static int64_t start_light_sleep(void);

      /**
      * @brief start deep sleep
      */
      static void start_deep_sleep(void);

   private:
      static inline void take(void) {
         xSemaphoreTake(mutex, portMAX_DELAY); // portMAX_DELAY means infinite waiting
      }

      static inline void give(void) {
         xSemaphoreGive(mutex);
      }

      static xSemaphoreHandle mutex;
      static unsigned int light_cnt; // current value of counting dow for light sleep
      static unsigned int deep_cnt;  // current value of counting dow for deep sleep
      static unsigned int light_max; // max value of counting dow for light sleep
      static unsigned int deep_max;  // max value of counting dow for deep sleep

   private:
      // singleton
      cmscSleep(void) {    // private default constructor
         if(mutex == NULL) {
            mutex = xSemaphoreCreateMutex();
            std::cout<<"cmscSleep::getInstance mutex created\n";
         }
      }

      cmscSleep(const cmscSleep&); // private copy constructor
      const cmscSleep operator = (const cmscSleep&); // private assignment operator
};


#if 0
/*
ref info about sleep from web

http://www.elecfans.com/d/1086267.html
ESP32具有以下功能：省电模式，称为“深度睡眠”。在这种模式下，CPU，大多数RAM和所有数字时钟外围设备都将关闭。
芯片上唯一仍可连接的部分是RTC控制器，RTC外设（包括ULP协处理器）和RTC存储器
进入深度睡眠模式之前，可以随时设置唤醒源。

有五种唤醒ESP32的方法：

•计时器              ESP_SLEEP_WAKEUP_TIMER
•外部唤醒（ext0）     ESP_SLEEP_WAKEUP_EXT0
•外部唤醒（ext1）     ESP_SLEEP_WAKEUP_EXT1
•ULP协处理器唤醒      ESP_SLEEP_WAKEUP_ULP
•触摸板               ESP_SLEEP_WAKEUP_TOUCHPAD

TIMER
RTC控制器具有内置的计时器，在预定的时间段后，可用于激活芯片。时间以微秒精度指定。
esp_deep_sleep_enable_timer_wakeup（ uint64_t time_in_us ）

EXT0  (single pin)
当RTC GPIO之一进入预定义的逻辑级别时，RTC IO模块包含触发警报的逻辑。 RTC IO是RTC外设电源域的一部分，因此，如果请求此激活源，则在深度睡眠期间RTC外设将保持活动状态。
esp_deep_sleep_enable_ext0_wakeup（ gpio_num_t gpio_num ， int level）           // note: it's number of gpio
gpio_num 使用的GPIO编号作为激活源。只能使用RTC功能的GPIO：0，2，4，12-15，25-27，32-39。
level    （0 = LOW，1 =高）

EXT1 (multiple pin)
esp_deep_sleep_enable_ext1_wakeup（uint64_t mask， esp_ext1_wakeup_mode_t mode） // note: it's mask of gpio
mask 会导致激活的GPIO编号的位掩码。此位图中只能使用启用RTC的GPIO：0，2，4，12-15，25-27，32-39。
mode  选择用于确定激活条件的逻辑功能：
• ESP_EXT1_WAKEUP_ALL_LOW ：在所有选定的GPIO都为LOW时唤醒
• ESP_EXT1_WAKEUP_ANY_HIGH ：在任何选定的GPIO为HIGH时唤醒


TOUCHPAD 触摸板
RTC控制器包含使用电容式触摸传感器触发警报的逻辑。但是，触针的定义不同。我们必须对每个所需的引脚使用触摸中断。
设置中断后，我们启用唤醒模式以使用传感器。
//Configure Touchpad as wakeup source
esp_sleep_enable_touchpad_wakeup();

Eg: TOUCH 7 = GPIO27 = PIN 16

more about touch pad wake up refer to web link



设置唤醒模式后，只需一个命令即可将ESP32置于深度睡眠模式（花费2.5μA或更小）。我在这里强调，这笔费用来自ESP芯片，而不是印版，因为后者花费更多。
esp_deep_sleep_start（）;
从该命令开始，ESP32进入睡眠状态，并且不执行下一行
重要说明：必须在执行上述命令之前进行所有唤醒设置。



https://blog.csdn.net/finedayforu/article/details/108547038

code demo:
esp_sleep_enable_timer_wakeup(20000000);
esp_deep_sleep_start();


https://zhuanlan.zhihu.com/p/130992695

5种功耗模式分别为：
Active 模式、Modem-sleep 模式、Light-sleep 模式、Deep-sleep 模式、休眠模式.
除了Active模式外其他四种模式都属于低功耗模式，按照顺序越往后功耗越低，工作的模块越少。
      modem-sleep的电流消耗为30mA～3mA
      Light-sleep的电流消耗为800uA
      deep-sleep模式的电流消耗为6.5uA
      休眠模式的电流消耗最低可以达到4.5uA (hebernate)

wake_up_source       light_sleep          deep_sleep      hebernate
EXT0                    Y                    Y
EXT1                    Y                    Y              Y
GPIO                    Y                    Y
RTC                     Y                    Y              Y
SDIO                    Y
WiFi                    Y
UART0                   Y
UART1                   Y

( Y = yes; otherwise = no)


在 Deep-sleep 状态下，GPIO 电平状态可以保持，具有 2 μA 的驱动能⼒。
esp_err_t gpio_hold_en(gpio_num_t gpio_num)  // enable individual gpio
void gpio_deep_sleep_hold_en(void) // enable all gpios

esp_err_t gpio_hold_dis(gpio_num_t gpio_num);
void gpio_deep_sleep_hold_dis(void);

*/
#endif

// file：mscSleep.cpp

#include "allSystem.h"

/*
   leon.li@mattel.com

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

xSemaphoreHandle cmscSleep::mutex = NULL;
unsigned int cmscSleep::light_cnt = 30;
unsigned int cmscSleep::deep_cnt = 60;
unsigned int cmscSleep::light_max = 30;
unsigned int cmscSleep::deep_max = 60;

mscWakeup_source cmscSleep::show_wakeup_source(void) {
   int rc = esp_sleep_get_wakeup_cause();

   std::string s1 = "ESP wakeup source: ";
   std::string s2;
   switch (rc) {
      case ESP_SLEEP_WAKEUP_UNDEFINED: {
         s2 = "undefined";
         break;
      }
      case ESP_SLEEP_WAKEUP_EXT0: {
         s2 = "ext0";
         break;
      }
      case ESP_SLEEP_WAKEUP_EXT1: {
         s2 = "ext1";
         break;
      }
      case ESP_SLEEP_WAKEUP_TIMER: {
         s2 = "timer";
         break;
      }
      case ESP_SLEEP_WAKEUP_TOUCHPAD: {
         s2 = "touchpad";
         break;
      }
      case ESP_SLEEP_WAKEUP_ULP: {
         s2 = "ulp";
         break;
      }
      case ESP_SLEEP_WAKEUP_GPIO: {
         s2 = "gpio";
         break;
      }
      case ESP_SLEEP_WAKEUP_UART: {
         s2 = "uart";
         break;
      }
      default: {
         rc = ESP_SLEEP_WAKEUP_UNDEFINED;
         s2 = "undefined";
         break;
      }
   }

   s1 += s2;
   std::cout<<s1<<" : "<<rc<<std::endl;
   return mscWakeup_source(rc);
}

int64_t cmscSleep::start_light_sleep(void) {
   take();
   std::cout<<"\n\ngo to light sleep...\n\n";
   vTaskDelay(1);
   uart_tx_wait_idle(CONFIG_ESP_CONSOLE_UART_NUM);

   int64_t us = esp_timer_get_time();
   esp_light_sleep_start();
   us = esp_timer_get_time() - us;

   vTaskDelay(200 / portTICK_PERIOD_MS);
   int ms = us / 1000;
   float sec = (float)ms / 1000.0;
   printf("\n\nlight sleep for %d ms, %.3f sec\n", ms, sec);
   give();
   return us;
}

void cmscSleep::start_deep_sleep(void) {
   take();
   std::cout<<"\n\ngo to deep sleep...\n\n";
   vTaskDelay(1);
   uart_tx_wait_idle(CONFIG_ESP_CONSOLE_UART_NUM);   
   esp_deep_sleep_start(); // chip will restart ...
   // vTaskDelay(200 / portTICK_PERIOD_MS);
   // give();
}

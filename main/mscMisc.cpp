// file：mscMisc.cpp

#include "allSystem.h"

/*
   leon.li@mattel.com

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

bool _cmscPwrUp_::isPwrUp = false;


mscReset_reason _cmscPwrUp_::reset_reason(void) {
   int rc = esp_reset_reason();

   std::string s1 = "ESP reset reason: ";
   std::string s2;
   switch (rc) {
      case ESP_RST_UNKNOWN: {
         s2 = "unknown";
         break;
      }
      case ESP_RST_POWERON: {
         s2 = "power on";
         break;
      }
      case ESP_RST_EXT: {
         s2 = "ext pin";
         break;
      }
      case ESP_RST_SW: {
         s2 = "esp_restart";
         break;
      }
      case ESP_RST_PANIC: {
         s2 = "exception";
         break;
      }
      case ESP_RST_INT_WDT: {
         s2 = "WDT";
         break;
      }
      case ESP_RST_TASK_WDT: {
         s2 = "task WDT";
         break;
      }
      case ESP_RST_WDT: {
         s2 = "other WDT";
         break;
      }
      case ESP_RST_DEEPSLEEP: {
         s2 = "exiting deep sleep";
         break;
      }
      case ESP_RST_BROWNOUT: {
         s2 = "brown out";
         break;
      }
      case ESP_RST_SDIO: {
         s2 = "over SDIO";
         break;
      }
      default: {
         rc = ESP_RST_UNKNOWN;
         s2 = "unknown";
         break;
      }
   }

   s1 += s2;
   std::cout<<s1<<" : "<<rc<<std::endl;
   return mscReset_reason(rc);
}

#define XOR_CR_START 0xCD
#define XOR_CR_INC   1
void _cmscCopyright_::show(void) {
   const static unsigned char copyright[] = {
      '\a' ^ (XOR_CR_START + XOR_CR_INC * 0),
      '\n' ^ (XOR_CR_START + XOR_CR_INC * 1),
      '\n' ^ (XOR_CR_START + XOR_CR_INC * 2),
      '\n' ^ (XOR_CR_START + XOR_CR_INC * 3),
      '\n' ^ (XOR_CR_START + XOR_CR_INC * 4),
      '\t' ^ (XOR_CR_START + XOR_CR_INC * 5),
      '\t' ^ (XOR_CR_START + XOR_CR_INC * 6),
      '\t' ^ (XOR_CR_START + XOR_CR_INC * 7),
      '\t' ^ (XOR_CR_START + XOR_CR_INC * 8),
      '!' ^ (XOR_CR_START + XOR_CR_INC * 9),
      '!' ^ (XOR_CR_START + XOR_CR_INC * 10),
      '!' ^ (XOR_CR_START + XOR_CR_INC * 11),
      ' ' ^ (XOR_CR_START + XOR_CR_INC * 12),
      'c' ^ (XOR_CR_START + XOR_CR_INC * 13),
      'o' ^ (XOR_CR_START + XOR_CR_INC * 14),
      'p' ^ (XOR_CR_START + XOR_CR_INC * 15),
      'y' ^ (XOR_CR_START + XOR_CR_INC * 16),
      'r' ^ (XOR_CR_START + XOR_CR_INC * 17),
      'i' ^ (XOR_CR_START + XOR_CR_INC * 18),
      'g' ^ (XOR_CR_START + XOR_CR_INC * 19),
      'h' ^ (XOR_CR_START + XOR_CR_INC * 20),
      't' ^ (XOR_CR_START + XOR_CR_INC * 21),
      ' ' ^ (XOR_CR_START + XOR_CR_INC * 22),
      '!' ^ (XOR_CR_START + XOR_CR_INC * 23),
      '!' ^ (XOR_CR_START + XOR_CR_INC * 24),
      '!' ^ (XOR_CR_START + XOR_CR_INC * 25),
      ' ' ^ (XOR_CR_START + XOR_CR_INC * 26),
      'l' ^ (XOR_CR_START + XOR_CR_INC * 27),
      'e' ^ (XOR_CR_START + XOR_CR_INC * 28),
      'o' ^ (XOR_CR_START + XOR_CR_INC * 29),
      'n' ^ (XOR_CR_START + XOR_CR_INC * 30),
      '.' ^ (XOR_CR_START + XOR_CR_INC * 31),
      'l' ^ (XOR_CR_START + XOR_CR_INC * 32),
      'i' ^ (XOR_CR_START + XOR_CR_INC * 33),
      '@' ^ (XOR_CR_START + XOR_CR_INC * 34),
      'm' ^ (XOR_CR_START + XOR_CR_INC * 35),
      'a' ^ (XOR_CR_START + XOR_CR_INC * 36),
      't' ^ (XOR_CR_START + XOR_CR_INC * 37),
      't' ^ (XOR_CR_START + XOR_CR_INC * 38),
      'e' ^ (XOR_CR_START + XOR_CR_INC * 39),
      'l' ^ (XOR_CR_START + XOR_CR_INC * 40),
      '.' ^ (XOR_CR_START + XOR_CR_INC * 41),
      'c' ^ (XOR_CR_START + XOR_CR_INC * 42),
      'o' ^ (XOR_CR_START + XOR_CR_INC * 43),
      'm' ^ (XOR_CR_START + XOR_CR_INC * 44),
      '\n' ^ (XOR_CR_START + XOR_CR_INC * 45),
      '\n' ^ (XOR_CR_START + XOR_CR_INC * 46),
      '\n' ^ (XOR_CR_START + XOR_CR_INC * 47),
      '\n' ^ (XOR_CR_START + XOR_CR_INC * 48),
   };

    int len = (sizeof(copyright) / sizeof(unsigned char));
    unsigned char *cr = NULL;
    _cmscNewMutex_::take();
    cr = new unsigned char[len+1];
    _cmscNewMutex_::give();
    if(cr != NULL) {
        for(int i = 0; i < len; i++) {
            cr[i] = (copyright[i] ^ (XOR_CR_START + XOR_CR_INC * i));
        }
        cr[len] = '\0';
        vTaskDelay(10);
        printf("%s", cr);
        delete []cr;
        cr = NULL;
    }else{
        esp_restart(); // should not happen
    }
}

std::string _cmscCopyright_::wifissid(void) {
   const static unsigned char ssid[] = {
      'M' ^ (XOR_CR_START + XOR_CR_INC * 0),
      '&' ^ (XOR_CR_START + XOR_CR_INC * 1),
      'M' ^ (XOR_CR_START + XOR_CR_INC * 2),
      ' ' ^ (XOR_CR_START + XOR_CR_INC * 3),
      'W' ^ (XOR_CR_START + XOR_CR_INC * 4),
      'i' ^ (XOR_CR_START + XOR_CR_INC * 5),
      'F' ^ (XOR_CR_START + XOR_CR_INC * 6),
      'i' ^ (XOR_CR_START + XOR_CR_INC * 7),
      ' ' ^ (XOR_CR_START + XOR_CR_INC * 8),
      'l' ^ (XOR_CR_START + XOR_CR_INC * 9),
      'e' ^ (XOR_CR_START + XOR_CR_INC * 10),
      'o' ^ (XOR_CR_START + XOR_CR_INC * 11),
      'n' ^ (XOR_CR_START + XOR_CR_INC * 12),
      '.' ^ (XOR_CR_START + XOR_CR_INC * 13),
      'l' ^ (XOR_CR_START + XOR_CR_INC * 14),
      'i' ^ (XOR_CR_START + XOR_CR_INC * 15),
      '@' ^ (XOR_CR_START + XOR_CR_INC * 16),
      'm' ^ (XOR_CR_START + XOR_CR_INC * 17),
      'a' ^ (XOR_CR_START + XOR_CR_INC * 18),
      't' ^ (XOR_CR_START + XOR_CR_INC * 19),
      't' ^ (XOR_CR_START + XOR_CR_INC * 20),
      'e' ^ (XOR_CR_START + XOR_CR_INC * 21),
      'l' ^ (XOR_CR_START + XOR_CR_INC * 22),
      '.' ^ (XOR_CR_START + XOR_CR_INC * 23),
      'c' ^ (XOR_CR_START + XOR_CR_INC * 24),
      'o' ^ (XOR_CR_START + XOR_CR_INC * 25),
      'm' ^ (XOR_CR_START + XOR_CR_INC * 26),
   };

   std::string str = "...";
   int len = (sizeof(ssid) / sizeof(unsigned char));
   unsigned char *cr = NULL;
   _cmscNewMutex_::take();
   cr = new unsigned char[len+1];
   _cmscNewMutex_::give();
   if(cr != NULL) {
      for(int i = 0; i < len; i++) {
         cr[i] = (ssid[i] ^ (XOR_CR_START + XOR_CR_INC * i));
      }
      cr[len] = '\0';
      vTaskDelay(10);
      str = (char *)cr;
      delete []cr;
      cr = NULL;
   }else{
      esp_restart(); // should not happen
   }
    return str;
}

#undef XOR_CR_START
#undef XOR_CR_INC

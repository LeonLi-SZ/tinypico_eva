// file：mscGPIO.h

#pragma once


/*
   leon.li@mattel.com

   refer to ESP-IDF v4.0 demo project : gpio

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "allSystem.h"

enum class mscSinglepin_wakeup_level {
   low = 0,
   high = 1,
};


/*
use below contents from ESP-IDF API code for mscGPIO_irq_level definition

//Keep the LEVELx values as they are here; they match up with (1<<level)
#define ESP_INTR_FLAG_LEVEL1		(1<<1)	///< Accept a Level 1 interrupt vector (lowest priority)
#define ESP_INTR_FLAG_LEVEL2		(1<<2)	///< Accept a Level 2 interrupt vector
#define ESP_INTR_FLAG_LEVEL3		(1<<3)	///< Accept a Level 3 interrupt vector
#define ESP_INTR_FLAG_LEVEL4		(1<<4)	///< Accept a Level 4 interrupt vector
#define ESP_INTR_FLAG_LEVEL5		(1<<5)	///< Accept a Level 5 interrupt vector
#define ESP_INTR_FLAG_LEVEL6		(1<<6)	///< Accept a Level 6 interrupt vector
#define ESP_INTR_FLAG_NMI			(1<<7)	///< Accept a Level 7 interrupt vector (highest priority)
#define ESP_INTR_FLAG_SHARED		(1<<8)	///< Interrupt can be shared between ISRs
#define ESP_INTR_FLAG_EDGE			(1<<9)	///< Edge-triggered interrupt
#define ESP_INTR_FLAG_IRAM			(1<<10)	///< ISR can be called if cache is disabled
#define ESP_INTR_FLAG_INTRDISABLED	(1<<11)	///< Return with this interrupt disabled
*/

/**
* @brief (whole) GPIO irq priority level Eg: level1,2,3
*/
enum class mscGPIO_irq_level {
    level1_lowest = ESP_INTR_FLAG_LEVEL1,
    level2 = ESP_INTR_FLAG_LEVEL2,
    level3 = ESP_INTR_FLAG_LEVEL3,
    // level4 = ESP_INTR_FLAG_LEVEL4,            not allowed to use. I don't know why
    // level5 = ESP_INTR_FLAG_LEVEL5,            not allowed to use
    // level6 = ESP_INTR_FLAG_LEVEL6,            not allowed to use
    // levelNMI_highest = ESP_INTR_FLAG_NMI,     not allowed to use
};


/*
use below contents from ESP-IDF API code for mscGPIO_irq_type & mscGPIO_wakeup_type definition
typedef enum {
    GPIO_INTR_DISABLE = 0,     //  < Disable GPIO interrupt
    GPIO_INTR_POSEDGE = 1,     //  < GPIO interrupt type : rising edge
    GPIO_INTR_NEGEDGE = 2,     //  < GPIO interrupt type : falling edge
    GPIO_INTR_ANYEDGE = 3,     //  < GPIO interrupt type : both rising and falling edge
    GPIO_INTR_LOW_LEVEL = 4,   //  < GPIO interrupt type : input low level trigger
    GPIO_INTR_HIGH_LEVEL = 5,  //  < GPIO interrupt type : input high level trigger
    GPIO_INTR_MAX,
} gpio_int_type_t;
*/

/**
* @brief individual GPIO pin irq detect edge type Eg: raising edge or fall edge or both edge
*/
enum class mscGPIO_irq_type {
    disable = GPIO_INTR_DISABLE,
    rising_edge = GPIO_INTR_POSEDGE,
    falling_edge = GPIO_INTR_NEGEDGE,
    both_edge = GPIO_INTR_ANYEDGE,
    // low_level = GPIO_INTR_LOW_LEVEL,   I don't think it's useful
    // high_level = GPIO_INTR_HIGH_LEVEL,  I don't think it's useful
};

/**
* @brief individual GPIO pin wakeup type. only support high or low level, no edge support, no both level support
*/
enum class mscGPIO_wakeup_type {
    low_level = GPIO_INTR_LOW_LEVEL,
    high_level = GPIO_INTR_HIGH_LEVEL,
};

/**
* @brief TinyPICO board has below pins
*/
enum class mscGPIO_pin_no {
    adc2ch8_dac1_g25_p14 = 25,  // g = gpio #, p = chip pin #
    adc2ch9_dac2_g26_p15 = 26,
    adc2ch7_touch7_g27_p16 = 27,
    adc2ch3_touch3_g15_p21 = 15,
    adc2ch6_touch6_g14_p17 = 14,
    adc2ch0_touch0_g4_p24 = 4,
    approxbatvolt_g35 = 35,
    chargingstate_g34 = 34,
    adc1ch5_touch8_g33_p13 = 33,
    adc1ch4_touch9_g32_p12 = 32,
    sda_g21_p42 = 21,
    scl_g22_p39 = 22,
    ss_g5_p34 = 5,
    sck_g18_p35 = 18,
    miso_g9_p38 = 19,
    mosi_g23_p36 = 23,

    pin_nc = GPIO_NUM_NC,

    rgbled_pwr_g13 = 13,
    rgbled_dat_g2 = 2,
    rgbled_clk_g12 = 12,
};


/**
* @brief MSC class for control GPIO
 */
class cmscGPIO {
    public:

        /**
        * @brief constructor
        * @param gpio_no GPIO#. eg: 25 is for GPIO25 which is pin 14 of TinyPICO board
        * @return none
        */
        inline cmscGPIO(mscGPIO_pin_no gpio_no) {
            this->gpio_no = gpio_no;
        }

        /**
        * @brief constructor
        * @return none
        */
        inline cmscGPIO(void) {
            gpio_no = mscGPIO_pin_no::pin_nc; // Use to signal not connected to S/W
         }

        /**
        * @brief assign pin number
        * @param gpio_no GPIO#. eg: 25 is for GPIO25 which is pin 14 of TinyPICO board
        * @return reference of object itself
        */
        cmscGPIO& pinNo(mscGPIO_pin_no gpio_no);

        /**
        * @brief set the pin to output (dft = no pull-up/down aka push-pull)
        * @return reference of object itself
        */
        cmscGPIO& setOutput(void);

        /**
        * @brief set the pin to input (dft = pull-up)
        * @return reference of object itself
        */
        cmscGPIO& setInput(void);

        /**
        * @brief configure internal pull-up
        * @param en true for enable, false for disable
        * @return reference of object itself
        */
        cmscGPIO& pullUp(bool en);

        /**
        * @brief configure internal pull-down
        * @param en true for enable, false for disable
        * @return reference of object itself
        */
        cmscGPIO& pullDown(bool en);

        /**
        * @brief set the pin to high
        * @return none
        */
        inline void high(void) {
            gpio_set_level((gpio_num_t)gpio_no, 1);
        }

        /**
        * @brief set the pin to low
        * @return none
        */
        inline void low(void) {
            gpio_set_level((gpio_num_t)gpio_no, 0);
        }

        /**
        * @brief read input pin level
        * @return input pin level
        */
        inline int readPin(void) {
            return gpio_get_level((gpio_num_t)gpio_no);
        }

        /**
        * @brief install an irq handler for the pin
        * @param handler static void IRAM_ATTR gpio_isr_handler(void* arg) {...}
        * @param type raising / falling edge etc... (it's for individual GPIO pin)
        * @param level priority level of 1...3 (it's for whole GPIO irq priority level)
        */
        void installIrq(void (*handler)(void *arg), mscGPIO_irq_type type, mscGPIO_irq_level level);
        /*
        use IRAM_ATTR in front of the irq routine should be faster repond I think
        I've tested. without IRAM_ATTR still OK
		but ESP-IDF API recommend to use IRAM_ATTR as prefix of the irq handler
        */

		/**
        * @brief install an irq handler for the pin with both_edge and GPIO irq priority level 3 (frequency used case I think)
        * @param handler static void IRAM_ATTR gpio_isr_handler(void* arg) {...}
        */
		inline void installIrq(void (*handler)(void *arg)) {
			installIrq(handler, mscGPIO_irq_type::both_edge, mscGPIO_irq_level::level3);
		}

		/**
        * @brief enable a specified type of irq detect on the pin
        * @param type raising / falling edge etc...
        */
        inline void enablePinIrq(mscGPIO_irq_type type) {
            gpio_set_intr_type((gpio_num_t)gpio_no, (gpio_int_type_t)type);
        }

		/**
        * @brief disable any irq detect on the pin
        */
        inline void disablePinIrq(void) {
            gpio_set_intr_type((gpio_num_t)gpio_no, (gpio_int_type_t)mscGPIO_irq_type::disable);
        }

		/**
        * @brief enable a specified type of wake up on the pin (esp_sleep_enable_gpio_wakeup() will be called also).
        * both for all IOs: RTC IOs and digital IOs. but only for light sleep wake up.
        * more detail refer ESP-IDF esp_sleep.h
        * to disable all wakeup source, call:
        * cmscSleep::disable_all_wakeup_source().
        * @param type high or low level. no edge support. no both level support
        */
        inline void enablePinWakeup(mscGPIO_wakeup_type type) {
            gpio_wakeup_enable((gpio_num_t)gpio_no, (gpio_int_type_t)type);
            esp_sleep_enable_gpio_wakeup();
        }

		/**
        * @brief disable a specified type of wake up on the pin
        */
        inline void disablePinWakeup(void) {
            gpio_wakeup_disable((gpio_num_t)gpio_no);
        }

		/**
        * @brief enable a specified type of wake up on the pin from deep sleep
        * only for RTC IOs (0,2,4,12-15,25-27,32-39).
        * to disable all wakeup source, call:
        * cmscSleep::disable_all_wakeup_source().
        * more detail refer ESP-IDF esp_sleep.h
        * @param level high or low level. no edge support. no both level support
        */
        inline void enablePinWakeupDeepSleep(mscSinglepin_wakeup_level level) {
            esp_sleep_enable_ext0_wakeup((gpio_num_t)gpio_no, (int)level);
        }

		/**
        * @brief Enable gpio pad hold function during deep sleep.
        * in output mode: the output level of the pad will be force locked and can not be changed.
        * in input mode: the input value read will not change, regardless the changes of input signal.
        * if you want to enable all GPIOs hold function. use cmscSleep::enable_all_gpio_hold().
        */
        inline void enablePinHold(void) {
             gpio_hold_en((gpio_num_t)gpio_no);
        }

		/**
        * @brief Enable gpio pad hold function during deep sleep.
        * if you want to enable all GPIOs hold function. use cmscSleep::disable_all_gpio_hold()
        */
        inline void disablePinHold(void) {
            gpio_hold_dis((gpio_num_t)gpio_no);
        }

    private:
        mscGPIO_pin_no gpio_no; // GPIO#. eg: 25 is for GPIO25 which is pin 14 of TinyPICO board
        static bool isIrqServiceInstalled; // prevent multi install (but looks like multi install still OK)
};

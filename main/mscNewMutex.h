// file：mscNewMutex.h

#pragma once


/*
   leon.li@mattel.com

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "allSystem.h"

// I don't think we need this, but just keep it anyway

/**
* @brief internal use only. do NOT use it external. must get this mutex before 'new' action
*/
class _cmscNewMutex_ {
    public:
        _cmscNewMutex_(void) {
            if(mutex == NULL) {
                mutex = xSemaphoreCreateMutex();
                std::cout<<"_cmscNewMutex_ mutex created\n";
            }
        }

    protected:
        static inline void take(void) {
            xSemaphoreTake(mutex, portMAX_DELAY); // portMAX_DELAY means infinite waiting
        }

        static inline void give(void) {
            xSemaphoreGive(mutex);
        }

    private:
        /**
        * @brief mutex gard
        */
        static xSemaphoreHandle mutex;
};

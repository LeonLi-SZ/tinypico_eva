// file：mscRGBled.cpp

#include "allSystem.h"

static constexpr mscGPIO_pin_no TINYPICO_RGB_LED_PWR = mscGPIO_pin_no::rgbled_pwr_g13;
static constexpr mscGPIO_pin_no TINYPICO_RGB_LED_DAT = mscGPIO_pin_no::rgbled_dat_g2;
static constexpr mscGPIO_pin_no TINYPICO_RGB_LED_CLK = mscGPIO_pin_no::rgbled_clk_g12;


cmscRGBled::cmscRGBled() {
  std::cout<<"cmscRGBled::cmscRGBled created\n";
  r = 0, g = 0, b = 0;
  bri = (B1110_0000 | 0); // MSB 3 bits = 111. see LED DS

  pwr.pinNo(TINYPICO_RGB_LED_PWR).setInput();
  dat.pinNo(TINYPICO_RGB_LED_DAT).setInput();
  clk.pinNo(TINYPICO_RGB_LED_CLK).setInput();

  mutex = xSemaphoreCreateMutex();
}

// ============== below are private functions ================ //

void cmscRGBled::updateLED(void) {
  // refer to RGB LED DS
  swspi_out(0), swspi_out(0), swspi_out(0), swspi_out(0);  // start frame
  swspi_out(bri);    // global = 111xxxxx   LSB 5 bits is brightness
  swspi_out(b);
  swspi_out(g);
  swspi_out(r);
  swspi_out(0xFF), swspi_out(0xFF), swspi_out(0xFF), swspi_out(0xFF);   // stop frame
}

void cmscRGBled::swspi_out(uint8_t n) {

  for (uint8_t i = 8; i--; n <<= 1)
  {
    if (n & B1000_0000) {
      dat.high();
    } else {
      dat.low();
    }

    clk.high();
    clk.low();
  }
}


void cmscRGBled::ctrl(int mode, uint8_t brightness, uint8_t r, uint8_t g, uint8_t b) {
  xSemaphoreTake(mutex, portMAX_DELAY); // portMAX_DELAY means infinite waiting

  // mode 0 = refresh only; 1 = power up; 2 = power down; 3 = set brightness, 4 = set r/g/b color
  switch (mode) {
    case 0: {
      updateLED();
      break;
    }
    case 1: {
      pwr.setOutput().low(); // see TinyPICO sch
      dat.setOutput();
      clk.setOutput();
      break;
    }
    case 2: {
      // power is off, all configure inside LED lost
      dat.setInput().pullUp(false);
      clk.setInput().pullUp(false);

      pwr.high();
      // pwr.setInput();  // pull-down can not cut off LED power. see TinyPICO schematic
      break;
    }
    case 3: {
      if(brightness >= 100) brightness = 100;
      brightness = brightness * 31 / 100; // map to [0, 31]

      bri = (brightness | B1110_0000);
      updateLED();
      break;
    }
    case 4: {
      this->r = r;
      this->g = g;
      this->b = b;
      updateLED();
      break;
    }
  }

  xSemaphoreGive(mutex);
}

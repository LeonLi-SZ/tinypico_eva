// file：mscGPIO.cpp

#include "allSystem.h"


cmscGPIO& cmscGPIO::pinNo(mscGPIO_pin_no gpio_no) {
    this->gpio_no = gpio_no;
    return *this;
}

cmscGPIO& cmscGPIO::setOutput(void) {
    const uint64_t GPIO_OUTPUT_PIN_SEL = (1ULL<<(int)gpio_no);
    gpio_config_t io_conf;

    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    //disable pull-up mode
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

    return *this;
}

cmscGPIO& cmscGPIO::setInput(void) {
    const uint64_t GPIO_OUTPUT_PIN_SEL = (1ULL<<(int)gpio_no);
    gpio_config_t io_conf;

    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_INPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    //disable pull-up mode
    io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

    return *this;
}

cmscGPIO& cmscGPIO::pullUp(bool en) {
    if(en) {
        gpio_pullup_en((gpio_num_t)gpio_no);
    }else{
        gpio_pullup_dis((gpio_num_t)gpio_no);
    }
    return *this;
}

cmscGPIO& cmscGPIO::pullDown(bool en) {
    if(en) {
        gpio_pulldown_en((gpio_num_t)gpio_no);
    }else{
        gpio_pulldown_dis((gpio_num_t)gpio_no);
    }
    return *this;
}

void cmscGPIO::installIrq(void  (*handler)(void *arg), mscGPIO_irq_type type, mscGPIO_irq_level level) {
    gpio_set_intr_type((gpio_num_t)gpio_no, (gpio_int_type_t)type);

    if(!isIrqServiceInstalled) { // prevent multi install (but looks like multi install still OK)
        isIrqServiceInstalled = true;
        gpio_install_isr_service((int)level);
    }

    gpio_isr_handler_add((gpio_num_t)gpio_no, handler, (void *)gpio_no);
}

bool cmscGPIO::isIrqServiceInstalled = false;


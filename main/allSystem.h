#pragma once


#include <stdio.h>


#include "freertos/FreeRTOS.h"
#include "freertos/FreeRTOSConfig.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/list.h"
#include "freertos/event_groups.h"
#include "freertos/timers.h"

#include "nvs_flash.h"
#include "nvs.h"

#include "driver/gpio.h"
#include "driver/i2c.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_sleep.h"
#include "esp32/rom/uart.h"
#include "esp_timer.h"



// std::string
#include <string>       // http://www.cplusplus.com/reference/string/string/
#include "string.h"     // char *strcpy(char* dest, const char *src); etc...

// std::cout
// std::in
// std::endl
#include <iostream>

#define bitmask(n) (1ul << n)

#include "bit256.h"

#include "mscNewMutex.h"
#include "mscMisc.h"
#include "mscGPIO.h"
#include "mscRGBled.h"
#include "mscLiteFIFO.h"
#include "mscEEPROM.h"
#include "mscIIC.h"
#include "mscSleep.h"



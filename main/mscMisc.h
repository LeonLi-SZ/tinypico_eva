// file：mscMisc.h

#pragma once


/*
   leon.li@mattel.com

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "allSystem.h"


/*
use below contents from ESP-IDF API code for mscReset_reason definition
typedef enum {
    ESP_RST_UNKNOWN,    //!< Reset reason can not be determined
    ESP_RST_POWERON,    //!< Reset due to power-on event
    ESP_RST_EXT,        //!< Reset by external pin (not applicable for ESP32)
    ESP_RST_SW,         //!< Software reset via esp_restart
    ESP_RST_PANIC,      //!< Software reset due to exception/panic
    ESP_RST_INT_WDT,    //!< Reset (software or hardware) due to interrupt watchdog
    ESP_RST_TASK_WDT,   //!< Reset due to task watchdog
    ESP_RST_WDT,        //!< Reset due to other watchdogs
    ESP_RST_DEEPSLEEP,  //!< Reset after exiting deep sleep mode
    ESP_RST_BROWNOUT,   //!< Brownout reset (software or hardware)
    ESP_RST_SDIO,       //!< Reset over SDIO
} esp_reset_reason_t;
*/

/**
* @brief reset reason
*/
enum class mscReset_reason {
    UNKNOWN = ESP_RST_UNKNOWN,      // Reset reason can not be determined
    POWERON = ESP_RST_POWERON,      // Reset due to power-on event
    EXT = ESP_RST_EXT,              // Reset by external pin (not applicable for ESP32)
    SW = ESP_RST_SW,                // Software reset via esp_restart
    PANIC = ESP_RST_PANIC,          // Software reset due to exception/panic
    INT_WDT = ESP_RST_INT_WDT,      // Reset (software or hardware) due to interrupt watchdog
    TASK_WDT = ESP_RST_TASK_WDT,    // Reset due to task watchdog
    WDT = ESP_RST_WDT,              // Reset due to other watchdogs
    DEEPSLEEP = ESP_RST_DEEPSLEEP,  // Reset after exiting deep sleep mode
    BROWNOUT = ESP_RST_BROWNOUT,    // Brownout reset (software or hardware)
    SDIO = ESP_RST_SDIO,            // Reset over SDIO
};


/**
* @brief indicate power is up. main is running
 */
class _cmscPwrUp_ {
    public:

        /**
         * @brief set this bit to indicate power is up. recommend call it at the beginning of app_main (only can be set, no way to clear)
         */
        static inline void set(void) {
            isPwrUp = true;
        }

        /**
         * @brief set this bit to indicate power is up. recommend set in at the beginning of app_main. (only can be set, no way to clear)
         * @return check if power is up (app_main got control) Why we need this? EEPROM init. later than global var establish...
         */
        static inline bool get(void) {
            return isPwrUp;
        }

        /**
         * @brief show the reset reason on console and return reason code
         * @return reason code
         */
        static mscReset_reason reset_reason(void);

    private:
        static bool isPwrUp;
};

class _cmscCopyright_ : public _cmscNewMutex_ {
    public:
        static void show(void);
        static std::string wifissid(void);
};


// file：mscIIC.h

#pragma once


/*
   leon.li@mattel.com

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/


/**
* @brief internal use only. do NOT use it external. must get this mutex before access IIC
 */
class _cmscIIC_Mutex_ {
    public:
        _cmscIIC_Mutex_(void) {
            if(mutex == NULL) {
                mutex = xSemaphoreCreateMutex();
                std::cout<<"_cmscIIC_Mutex_ mutex created\n";
            }
        }

    protected:
        static inline void take(void) {
            xSemaphoreTake(mutex, portMAX_DELAY); // portMAX_DELAY means infinite waiting
        }

        static inline void give(void) {
            xSemaphoreGive(mutex);
        }

    private:
        /**
        * @brief mutex gard
        */
        static xSemaphoreHandle mutex;
};




// note: will use cmscSWiic and cmscHWiic


// same as later: will use cmscSWspi and cmscHWspi

















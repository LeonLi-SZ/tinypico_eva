// file: mscEEPROM.h

#pragma once

/*
   leon.li@mattel.com

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "allSystem.h"

/* =================================================
https://www.cnblogs.com/zy-cnblogs/p/13299474.html
在EPS32中已经将EEPROM弃用。对于ESP32上的新应用程序，建议使用NVS为首选项...

https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/storage/nvs_flash.html

https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32/api-reference/storage/nvs_flash.html

refer to ESP32-IDF API
C:\TinyPICO_ESP_IDF\components\nvs_flash\include\nvs.h
C:\TinyPICO_ESP_IDF\components\nvs_flash\include\nvs_flash.h

refer to ESP32-IDF example
nvs_blob_example_main.c
================================================= */

#define CMSC_EEPROM_STR_NOT_FOUND std::string("cmscEEPROM<std::string>:: string NOT FOUND")
#define CMSC_EEPROM_ACCESS_DLY 10

// all mscclass platform use this namespace for NVS
#define NVS_STORAGE_NAMESPACE "MSC_SZIH_NVS"

/**
* @brief internal use only. do NOT use it external
*/
class _cmscEEPROM_instance_  : protected _cmscNewMutex_ {
    public:
        _cmscEEPROM_instance_(void) {
            if(mutex == NULL) {
                mutex = xSemaphoreCreateMutex();
                std::cout<<"_cmscEEPROM_instance_ mutex created\n";
            }
        }

    protected:
        static inline void take(void) {
            xSemaphoreTake(mutex, portMAX_DELAY); // portMAX_DELAY means infinite waiting
        }

        static inline void give(void) {
            xSemaphoreGive(mutex);
        }

        /**
        * @brief if NVS (EEPROM) has been initiated. only need to do it once
        */
        static bool isInitiated;

    private:
        /**
        * @brief mutex gard
        */
        static xSemaphoreHandle mutex;
};


/* =================================================================================================
 if you define EEPROM var in global area, the EEPROM initiate procedure will be skipped
 by checking _cmscPwrUp_::get()
 make sure you define at least 1 other EEPROM var in local area to trigger the initiate proecdure
 before wriet/read them
==================================================================================================== */


/**
* @brief generic of EEPROM write/read (except for std::string etc. type) (std::string will use template specialization version)
*/
template <typename T>
class cmscEEPROM : protected _cmscEEPROM_instance_ {
    public:
        /**
        * @brief constructor
        * @param key a char * string to specify key for the var. Every var should have a unique key
        */
        cmscEEPROM(const char *key);

        /**
        * @brief write value to EEPROM
        * @param value content to write to EEPROM
        */
        void write(T value) {
            vTaskDelay(CMSC_EEPROM_ACCESS_DLY);
            _cmscEEPROM_instance_::take();
            set(value);
            _cmscEEPROM_instance_::give();
        }

        /**
        * @brief read content from EEPROM
        * @return content read out from EEPROM
        */
        T read(void) {
            vTaskDelay(CMSC_EEPROM_ACCESS_DLY);
            _cmscEEPROM_instance_::take();
            T value = get();
            _cmscEEPROM_instance_::give();
            return value;
        }

        /**
        * @brief operator = overide. make EEPROMvar.write(val_to_write); can be     EEPROMvar = val_to_write;
        */
        inline void operator = (T value) { // write
            write(value);
        }

        /**
        * @brief operator T() overide. make return_val = EEPROMvar.read(); can be   return_val = EEPROMvar;  sometimes must use   (T)EEPROMvar
        * @return content read out from EEPROM
        */
        inline operator T() { // read
            return read();
        }

    private:
        const char *key;  // a char * string to specify key for the var. Every var should have a unique key
        nvs_handle_t handle;

        void set(T value); // function as write (without mutex)
        T get(void);  // function as read (without mutex)
};

template<typename T>
cmscEEPROM<T>::cmscEEPROM(const char *key) {
    vTaskDelay(CMSC_EEPROM_ACCESS_DLY);
   this->key = key;

   if(!_cmscPwrUp_::get()) {
       std::cout<<"\n\npower is not up yet. cmscEEPROM::cmscEEPROM EEPROM initiate skipped\n\n";
       return;
   }

   _cmscEEPROM_instance_::take();
   esp_err_t err;
   if(!_cmscEEPROM_instance_::isInitiated) {
       _cmscEEPROM_instance_::isInitiated = true;
       std::cout<<"cmscEEPROM::cmscEEPROM EEPROM initiating...\n";
       err = nvs_flash_init();
       vTaskDelay(1);
       if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
            // NVS partition was truncated and needs to be erased
            // Retry nvs_flash_init
            ESP_ERROR_CHECK(nvs_flash_erase());
            vTaskDelay(1);
            err = nvs_flash_init();
        }
        ESP_ERROR_CHECK(err);
        vTaskDelay(1);
        std::cout<<"cmscEEPROM::cmscEEPROM EEPROM initiated.\n";
    }else{
        std::cout<<"cmscEEPROM::cmscEEPROM EEPROM already initiated in previous\n";
    }

    std::cout<<"cmscEEPROM::cmscEEPROM var created\n";
    _cmscEEPROM_instance_::give();
}

template<typename T>
void cmscEEPROM<T>::set(T value) {
   // open
   esp_err_t err = nvs_open(NVS_STORAGE_NAMESPACE, NVS_READWRITE, &handle);
   if(err != ESP_OK) {
      std::cout<<"!!! cmscEEPROM::set nvs_open ERROR !!!\n";
      return;
   }
   vTaskDelay(1);

   // write
   err = nvs_set_blob(handle, key, &value, sizeof(T));
   vTaskDelay(1);
   if(err != ESP_OK) {
      std::cout<<"!!! cmscEEPROM::set nvs_set_blob ERROR !!!\n";
   }

    // Commit
    err = nvs_commit(handle);
    if (err != ESP_OK) {
       std::cout<<"!!! cmscEEPROM::set commit ERROR !!!\n";
    }
    vTaskDelay(1);

    // Close
    nvs_close(handle);
}

template<typename T>
T cmscEEPROM<T>::get(void) {
    T value = T();

    // open
    esp_err_t err = nvs_open(NVS_STORAGE_NAMESPACE, NVS_READONLY, &handle);
    if(err != ESP_OK) {
        std::cout<<"!!! cmscEEPROM::get nvs_open ERROR !!!\n";
        return value;
    }
    vTaskDelay(1);

    // Read the size of memory space required for blob
    size_t required_size = 0;  // value will default to 0, if not set yet in NVS
    err = nvs_get_blob(handle, key, NULL, &required_size);
    if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND) {
        std::cout<<"!!! cmscEEPROM::get nvs_get_blob ERROR !!!\n";
        goto final_exit;
    }
    vTaskDelay(1);

    // Read previously saved blob if available
    if (required_size > 0) {
        typedef struct {
            T __t1;
            uint32_t __t2;  // I don't know why the ESP-IDF example requests 2 btyes more. I just follow it
        }__TMP_MSCEEPROM_T__;
        T *p;

        _cmscNewMutex_::take();
        p = (T*)(new __TMP_MSCEEPROM_T__());  // note: ESP-IDF API uses 'malloc', it has problem working with 'new' in C++
        _cmscNewMutex_::give();

        err = nvs_get_blob(handle, key, p, &required_size);
        vTaskDelay(1);
        if(err != ESP_OK) {
            delete p;
            std::cout<<"!!! cmscEEPROM::get read value ERROR !!!\n";
            goto final_exit;
        }else{
            value = *p;
            delete p;
            goto final_exit;
        }
    }else{
        std::cout<<"!!! cmscEEPROM::get size = 0 ERROR !!!\n";
    }

    final_exit:
    // Close
    nvs_close(handle);
    return value;
}

// ================= std::string template specialization version ================= //

/*
https://urldefense.com/v3/__https://blog.csdn.net/M_jianjianjiao/article/details/85220983__;!!Hd5UnLY!jDu_n7HDTXTktD8-z0QIt3IAiaUVv7kGJP24ej-EBT05yYfmHae2HoM355EmJQ$
关于模板的特化
template specialization
*/


/**
* @brief generic of EEPROM write/read (std::string template specialization version)
*/
template <>
class cmscEEPROM<std::string> : protected _cmscEEPROM_instance_ {
    public:
        /**
        * @brief constructor
        * @param key a char * string to specify key for the var. Every var should have a unique key
        */
        cmscEEPROM(const char *key) {
            vTaskDelay(CMSC_EEPROM_ACCESS_DLY);
            this->key = key;

            if(!_cmscPwrUp_::get()) {
                std::cout<<"\n\npower is not up yet. cmscEEPROM::cmscEEPROM<std::string> EEPROM initiate skipped\n\n";
                return;
            }

            _cmscEEPROM_instance_::take();
            esp_err_t err;
            if(!_cmscEEPROM_instance_::isInitiated) {
                _cmscEEPROM_instance_::isInitiated = true;

                std::cout<<"cmscEEPROM::cmscEEPROM<std::string> EEPROM initiating...\n";
                err = nvs_flash_init();
                vTaskDelay(1);
                if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
                    // NVS partition was truncated and needs to be erased
                    // Retry nvs_flash_init
                    ESP_ERROR_CHECK(nvs_flash_erase());
                    vTaskDelay(1);
                    err = nvs_flash_init();
                }
                ESP_ERROR_CHECK(err);
                vTaskDelay(1);
                std::cout<<"cmscEEPROM::cmscEEPROM<std::string> EEPROM initiated.\n";
            }else{
                std::cout<<"cmscEEPROM::cmscEEPROM<std::string> EEPROM already initiated in previous\n";
            }

            std::cout<<"cmscEEPROM::cmscEEPROM<std::string> var created\n";
            _cmscEEPROM_instance_::give();
        }

        /**
        * @brief write value to EEPROM
        * @param value content to write to EEPROM
        */
        void write(std::string value) {
            vTaskDelay(CMSC_EEPROM_ACCESS_DLY);
            _cmscEEPROM_instance_::take();
            set(value);
            _cmscEEPROM_instance_::give();
        }

        /**
        * @brief read content from EEPROM
        * @return content read out from EEPROM
        */
        std::string read(void) {
            vTaskDelay(CMSC_EEPROM_ACCESS_DLY);
            _cmscEEPROM_instance_::take();
            std::string value = get();
            _cmscEEPROM_instance_::give();
            return value;
        }

        /**
        * @brief operator = overide. make EEPROMvar.write(val_to_write); can be     EEPROMvar = val_to_write;
        */
        inline void operator = (std::string value) { // write
            write(value);
        }

        /**
        * @brief operator std::string() overide. make return_val = EEPROMvar.read(); can be   return_val = EEPROMvar;
        * @return content read out from EEPROM
        */
        inline operator std::string() { // read
            return read();
        }

    private:
        const char *key;  // a char * string to specify key for the var. Every var should have a unique key
        nvs_handle_t handle;

        /**
        * @brief private version of write (without mutex gurad)
        * @param value content to write to EEPROM
        */
        void set(std::string value) {
            int size = value.size();
            char *buf;

            _cmscNewMutex_::take();
            buf = new char[size];
            _cmscNewMutex_::give();

            // http://www.cplusplus.com/reference/string/string/copy/
            // size_t copy (char* s, size_t len, size_t pos = 0) const;
            value.copy(buf, size);

            // open
            esp_err_t err = nvs_open(NVS_STORAGE_NAMESPACE, NVS_READWRITE, &handle);
            if(err != ESP_OK) {
                std::cout<<"!!! cmscEEPROM<std::string>::set nvs_open ERROR !!!\n";
                delete[] buf;
                return;
            }
            vTaskDelay(1);

            // write
            err = nvs_set_blob(handle, key, buf, size);
            vTaskDelay(1);
            if(err != ESP_OK) {
                std::cout<<"!!! cmscEEPROM<std::string>::set nvs_set_blob ERROR !!!\n";
            }

            // Commit
            err = nvs_commit(handle);
            if (err != ESP_OK) {
                std::cout<<"!!! cmscEEPROM<std::string>::set commit ERROR !!!\n";
            }
            vTaskDelay(1);

            // Close
            nvs_close(handle);
            delete[] buf;
        }

        /**
        * @brief private version of read (without mutex gurad)
        * @return value content read out from EEPROM
        */
        std::string get(void) {
            std::string value = CMSC_EEPROM_STR_NOT_FOUND;

            // open
            esp_err_t err = nvs_open(NVS_STORAGE_NAMESPACE, NVS_READONLY, &handle);
            if(err != ESP_OK) {
                std::cout<<"!!! cmscEEPROM<std::string>::get nvs_open ERROR !!!\n";
                return value;
            }
            vTaskDelay(1);

            // Read the size of memory space required for blob
            size_t required_size = 0;  // value will default to 0, if not set yet in NVS
            err = nvs_get_blob(handle, key, NULL, &required_size);
            if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND) {
                std::cout<<"!!! cmscEEPROM<std::string>::get nvs_get_blob ERROR !!!\n";
                goto final_exit;
            }
            vTaskDelay(1);

            // Read previously saved blob if available
            if (required_size > 0) {
                char *buf = new char[required_size + 1];

                err = nvs_get_blob(handle, key, buf, &required_size);
                vTaskDelay(1);
                if(err != ESP_OK) {
                    delete []buf;
                    goto final_exit;
                    return value;
                }else{
                    buf[required_size] = '\0';
                    // http://www.cplusplus.com/reference/string/string/operator=/
                    // string& operator= (const char* s);
                    value = buf;
                    delete []buf;
                    goto final_exit;
                }
            }else{
                std::cout<<"!!! cmscEEPROM<std::string>::get size = 0 ERROR !!!\n";
                nvs_close(handle);
            }

            final_exit:
            // Close
            nvs_close(handle);
            return value;
        }
};

#undef CMSC_EEPROM_ACCESS_DLY
#undef NVS_STORAGE_NAMESPACE

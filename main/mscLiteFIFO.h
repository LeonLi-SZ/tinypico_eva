// file：mscLiteFIFO.h

#pragma once

/*
   leon.li@mattel.com

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "allSystem.h"


/* =============================================
  light weight linked list used as a FIFO

  Copyright Mattel 2020
  leon.li@mattel.com

  refer to
  https://github.com/ivanseidel/LinkedList

  LinkedList.h - V1.1 - Generic LinkedList implementation
  Works better with FIFO, because LIFO will need to
  search the entire List to find the last one;

  ============================================= */

// !!! do NOT seperated it into .h and .cpp files, it does NOT work !!!

template<class T>
struct ListNode
{
  T data;
  ListNode<T> *next;
};


/**
* @brief light-weight version of FIFO with mutex guard (actually uses list to make it)
 */
template <typename T>
class cmscLiteFIFOmutex : protected _cmscNewMutex_ {

  public:
    /**
    * @brief constructor
    */
    cmscLiteFIFOmutex();

    /**
    * @brief destructor
    */
    ~cmscLiteFIFOmutex();

    /**
    * @brief get size of elememt<T>
    * @return how many elements<T> inside the FIFO
    */
    inline int size() {
      return _size;
    }

    /**
    * @brief push an element<T> 'in' to the FIFO
    * @return size of FIFO after 'in' operation
    */
    int in(T);

    /**
    * @brief pon an element<T> 'out' of the FIFO
    * @return the element<T> which is being popped out. return a <T> created by its default constructor if FIFO already empty
    */
    T out();

    /**
    * @brief clear(delete) all element<T> of the FIFO
    */
    void clear();

  private:
    int _size;
    ListNode<T> *root;
    ListNode<T> *last;

    // Helps "get" method, by saving last position
    ListNode<T> *lastNodeGot;
    int lastIndexGot;
    // isCached should be set to FALSE
    // everytime the list suffer changes
    bool isCached;

    ListNode<T>* getNode(int index);

    T pop(); // I don't like to public this function

    /**
    * @brief mutex gard
    */
    xSemaphoreHandle mutex;
};

template<typename T>
cmscLiteFIFOmutex<T>::cmscLiteFIFOmutex()
{
  root = NULL;
  last = NULL;
  _size = 0;

  lastNodeGot = root;
  lastIndexGot = 0;
  isCached = false;

  mutex = xSemaphoreCreateMutex();
  std::cout<<"cmscLiteFIFOmutex<T>::cmscLiteFIFOmutex created\n";
}

// Clear Nodes and free Memory
template<typename T>
cmscLiteFIFOmutex<T>::~cmscLiteFIFOmutex()
{
  ListNode<T>* tmp;
  while (root != NULL)
  {
    tmp = root;
    root = root->next;
    delete tmp;
  }
  last = NULL;
  _size = 0;
  isCached = false;
}

template<typename T>
int cmscLiteFIFOmutex<T>::in(T _t) {
  xSemaphoreTake(mutex, portMAX_DELAY); // portMAX_DELAY means infinite waiting
  ListNode<T> *tmp;

  _cmscNewMutex_::take();
  tmp = new ListNode<T>();
  _cmscNewMutex_::give();

  tmp->data = _t;
  tmp->next = NULL;

  if (root) {
    // Already have elements inserted
    last->next = tmp;
    last = tmp;
  } else {
    // First element being inserted
    root = tmp;
    last = tmp;
  }

  _size++;
  isCached = false;

  xSemaphoreGive(mutex);
  return _size;
}

template<typename T>
T cmscLiteFIFOmutex<T>::out() {
  xSemaphoreTake(mutex, portMAX_DELAY); // portMAX_DELAY means infinite waiting
  if (_size <= 0) {
    xSemaphoreGive(mutex);
    return T(); // value created by default constructor of <T>
  }

  if (_size > 1) {
    ListNode<T> *_next = root->next;
    T ret = root->data;
    delete(root);
    root = _next;
    _size --;
    isCached = false;
    xSemaphoreGive(mutex);
    return ret;
  } else {
    // Only one left, then pop()
    //return pop();
    T tmp;
    tmp = pop();
    xSemaphoreGive(mutex);
    return tmp;
  }
}

template<typename T>
void cmscLiteFIFOmutex<T>::clear() {
  xSemaphoreTake(mutex, portMAX_DELAY); // portMAX_DELAY means infinite waiting
  while (size() > 0) {
      pop(); // out();  // original code uses out()
  }
  xSemaphoreGive(mutex);
}

// =============== below are private functions ================= //
template<typename T>
ListNode<T>* cmscLiteFIFOmutex<T>::getNode(int index) {

  int _pos = 0;
  ListNode<T>* current = root;

  // Check if the node trying to get is
  // immediatly AFTER the previous got one
  if (isCached && lastIndexGot <= index) {
    _pos = lastIndexGot;
    current = lastNodeGot;
  }

  while (_pos < index && current) {
    current = current->next;

    _pos++;
  }

  // Check if the object index got is the same as the required
  if (_pos == index) {
    isCached = true;
    lastIndexGot = index;
    lastNodeGot = current;

    return current;
  }

  return (ListNode<T>*)(NULL);
}

template<typename T>
T cmscLiteFIFOmutex<T>::pop() {
  if (_size <= 0)
    return T();

  isCached = false;

  if (_size >= 2) {
    ListNode<T> *tmp = getNode(_size - 2);
    T ret = tmp->next->data;
    delete(tmp->next);
    tmp->next = NULL;
    last = tmp;
    _size--;
    return ret;
  } else {
    // Only one element left on the list
    T ret = root->data;
    delete(root);
    root = NULL;
    last = NULL;
    _size = 0;
    return ret;
  }
}
